# Domain Adaptation by Unilateral Alignment in tensorflow and keras
["Missing-Class-Robust Domain Adaptation by Unilateral Alignment for Fault Diagnosis"](https://arxiv.org/pdf/2001.02015.pdf) introduced the unilateral alignment to overcome the problem of missing classes in the training target dataset.  
## Directory Structure
```
.
├── README.md
├── create_mnistm.py
├── data
│   ├── __init__.py
│   └── dataloader.py
├── model
│   ├── __init__.py
│   ├── dann.py
│   ├── dann_unilateral.py
│   └── element.py
├── run_experiment.py
└── utils.py

```


## Data Preparation
- Download the BSD500 dataset [Link to Download](http://www.eecs.berkeley.edu/Research/Projects/CS/vision/grouping/BSR/BSR_bsds500.tgz)
- Change the `BSD_PATH` in the file of `create_mnistm.py` for your downloaded dataset
- Run `create_mnistm.py` to create the mnistm dataset
```
python create_mnistm.py
```
## Usage
- Change the `MNIST_DATA_PATH` for the path of the `mnist_m_data.pkl`
- Start training the DANN or DANN-Unilateral model with different training mode and the number of missing classes

For example:
```
python \
run_experiment.py \
--mode='dann' \
--model='DANNUni' \
--missing_num=9 \
--batch_size=128 \
--epochs=50 
```

## Result
### DANN-Unilateral
![DANNUni](/uploads/a512c21510b5932336f0df0c7182487a/DANNUni.png)
### DANN
![DANN](/uploads/da6e938add438f19258224815e2bccb9/DANN.png)
