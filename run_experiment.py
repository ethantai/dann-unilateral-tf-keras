import argparse
import numpy as np
import tensorflow as tf
from sklearn.metrics import accuracy_score, confusion_matrix

try:
    import wandb
except ModuleNotFoundError:
    pass

from utils import UpdateCallback
from model import DANN, DANNUni
from data import DataLoader

tf.random.set_seed(1)
np.random.seed(1)

MNISTM_DATA_PATH = 'data/mnist_m_data.pkl'
MISSING_NUM = 0
EPOCHS = 50
BATCH_SIZE = 128
MODEL = "DANNUni"
MODE = "dann"


def _setup_parser():

    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument("--missing_num", type=int, default=MISSING_NUM)
    parser.add_argument("--batch_size", type=int, default=BATCH_SIZE)
    parser.add_argument("--epochs", type=int, default=EPOCHS)
    parser.add_argument("--model", type=str, choices=["DANNUni", "DANN"], default=MODEL)
    parser.add_argument("--mode", type=str, choices=["src", "tgt", "dann"], default=MODE)

    # weights and biases tool for tracking experiment
    parser.add_argument("--wandb", action="store_true", default=False)

    return parser


def main():
    parser = _setup_parser()
    args = parser.parse_args()

    data_loader = DataLoader(MNISTM_DATA_PATH, args.missing_num)
    # validation dataset uses only target data
    val_target_dataset = data_loader.getValTarget()
    val_dataset = val_target_dataset.batch(args.batch_size)

    ckpt_filepath = f"./training/{args.model}_{args.mode}_{args.missing_num}/best_model"
    ckpt_callbacks = tf.keras.callbacks.ModelCheckpoint(
        filepath=ckpt_filepath,
        save_weights_only=True,
        monitor='val_clf_acc',
        mode='max',
        save_best_only=True)
    callbacks = [ckpt_callbacks]

    # Use weights and biases tool to track our experiment
    if args.wandb:
        # initialize wandb logging to your project
        wandb.init(project='dann', notes=None)
        # log all experimental args to wandb
        wandb.config.update(args)
        callbacks.append(wandb.keras.WandbCallback())

    # Load training dataset depending on the training mode
    eager_mode = False
    if args.mode == 'dann':
        """
        training dataset -> source data and target data
        The eager mode has to be set to True because we have to update the adaptation parameter(lambda) in the gradient
        reversal layer every epoch
        """
        train_source_dataset = data_loader.getTrainSource()
        train_target_dataset = data_loader.getTrainTarget()
        train_dataset = tf.data.Dataset.zip((train_source_dataset, train_target_dataset.repeat())).batch(args.batch_size // 2).prefetch(tf.data.AUTOTUNE).shuffle(1001)
        # num_steps_per_epoch = len(train_dataset)
        # total_steps = num_steps_per_epoch * args.epochs
        eager_mode = True
        callbacks.append(UpdateCallback(args.epochs))

    elif args.mode == 'src':
        """
        training dataset -> only source data
        """
        train_source_dataset = data_loader.getTrainSource()
        train_dataset = train_source_dataset.batch(args.batch_size)

    elif args.mode == 'tgt':
        """
        training dataset -> only target data
        """
        train_target_dataset = data_loader.getTrainTarget()
        train_dataset = train_target_dataset.batch(args.batch_size)

    print(f"The number of missing classes: {args.missing_num}")
    print(f"Model: {args.model}")
    print(f"Training Mode: {args.mode}")
    print(f"Eager Mode: {eager_mode}")

    if args.model == 'DANN':

        model = DANN(training_mode=args.mode)
        model.compile(
            optimizer=tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.9),
            label_loss_fn=tf.keras.losses.CategoricalCrossentropy(),
            domain_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            run_eagerly=eager_mode,
        )
        model.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=args.epochs,
            callbacks=callbacks
        )
    else:
        # Data for training the pretrained feature extractor
        train_source_dataset = data_loader.getTrainSource()
        train_source_dataset = train_source_dataset.batch(args.batch_size)
        val_source_dataset = data_loader.getValSource()
        val_source_dataset = val_source_dataset.batch(args.batch_size)

        pretrained_model = DANN(training_mode="src")
        pretrained_model.compile(
            optimizer=tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.9),
            label_loss_fn=tf.keras.losses.CategoricalCrossentropy(),
            domain_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            run_eagerly=False,
        )
        print("========== Start training pretrained model ==========")
        pretrained_model.fit(
            train_source_dataset,
            validation_data=val_source_dataset,
            epochs=10
        )
        print("========== Pretrained feature extractor finished ==========")
        pretrained_feature_extractor = pretrained_model.feature_extractor
        model = DANNUni(training_mode=args.mode, pretrained_f=pretrained_feature_extractor)
        model.compile(
            optimizer=tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.9),
            label_loss_fn=tf.keras.losses.CategoricalCrossentropy(),
            domain_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            run_eagerly=eager_mode,
        )
        print("========== Start training DANN-Unilateral model ==========")
        model.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=args.epochs,
            callbacks=callbacks
        )
        print("========== Finished ==========")

    val_tgt_imgs = data_loader.tgt_imgs_val
    val_src_imgs = data_loader.src_imgs_val
    val_labels = data_loader.src_labels_val
    tgt_pred = model.predict(val_tgt_imgs)
    src_pred = model.predict(val_src_imgs)

    # Transform one-hot encoding to integer
    tgt_pred = tf.argmax(tgt_pred, axis=1).numpy()
    src_pred = tf.argmax(src_pred, axis=1).numpy()
    val_labels = tf.argmax(val_labels, axis=1).numpy()

    tgt_clf_acc = accuracy_score(val_labels, tgt_pred)
    src_clf_acc = accuracy_score(val_labels, src_pred)
    tgt_cm = confusion_matrix(val_labels, tgt_pred)
    src_cm = confusion_matrix(val_labels, src_pred)
    print(f"Evaluate the {args.model} model on the target data")
    print(f"Accuracy: {tgt_clf_acc}")
    print(f"Confusion Matrix:\n {tgt_cm}")
    print(f"Evaluate the {args.model} model on the source data")
    print(f"Accuracy: {src_clf_acc}")
    print(f"Confusion Matrix:\n {src_cm}")


if __name__ == '__main__':
    main()