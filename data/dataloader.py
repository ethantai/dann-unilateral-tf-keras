import numpy as np
import tensorflow as tf
import pickle as pkl
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical


class DataLoader:
    """A dataloader for preprocessing the data and loading training dataset and validation dataset"""
    def __init__(self, mnistm_filepath, missing_num):

        # Load MNIST
        (X_train, y_train), (X_test, y_test) = mnist.load_data()
        X_train = np.expand_dims(X_train, -1)
        X_test = np.expand_dims(X_test, -1)

        src_imgs_train = np.concatenate([X_train, X_train, X_train], axis=3).astype(np.float32)
        src_imgs_val = np.concatenate([X_test, X_test, X_test], axis=3).astype(np.float32)

        src_labels_train = np.array(y_train).astype(np.int32)
        src_labels_val = np.array(y_test).astype(np.int32)
        self.src_labels_train = to_categorical(src_labels_train).astype(np.float32)
        self.src_labels_val = to_categorical(src_labels_val).astype(np.float32)

        # Load MNIST-M
        ll = np.argmax(self.src_labels_train, axis=1)
        labels_mask = [kk in list(np.arange(10 - missing_num)) for kk in ll]
        # print(np.sum(labels_mask))
        mnistm = pkl.load(open(mnistm_filepath, 'rb'))

        tgt_imgs_train = mnistm['train'][labels_mask].astype(np.float32)
        self.tgt_labels_train = self.src_labels_train[labels_mask].astype(np.int32)

        tgt_imgs_val = mnistm['val'].astype(np.float32)

        # Compute pixel mean for normalizing data
        pixel_mean = np.vstack([src_imgs_train, tgt_imgs_train]).mean((0, 1, 2))
        # print(pixel_mean)

        self.src_imgs_train = (src_imgs_train - pixel_mean) / 255.0
        self.tgt_imgs_train = (tgt_imgs_train - pixel_mean) / 255.0
        self.src_imgs_val = (src_imgs_val - pixel_mean) / 255.0
        self.tgt_imgs_val = (tgt_imgs_val - pixel_mean) / 255.0

    def getTrainSource(self):
        """(training source images, training source labels)"""
        train_source_dataset = tf.data.Dataset.from_tensor_slices((self.src_imgs_train, self.src_labels_train)).shuffle(
            1000).prefetch(tf.data.AUTOTUNE).cache()
        return train_source_dataset

    def getValSource(self):
        """(validation source images, validation source labels)"""
        val_source_dataset = tf.data.Dataset.from_tensor_slices((self.src_imgs_val, self.src_labels_val)).shuffle(
            1000).prefetch(tf.data.AUTOTUNE)
        return val_source_dataset

    def getTrainTarget(self):
        """(training target images, training target labels)"""
        train_target_dataset = tf.data.Dataset.from_tensor_slices(
            (self.tgt_imgs_train, self.tgt_labels_train)).shuffle(
            1000).prefetch(tf.data.AUTOTUNE)
        return train_target_dataset

    def getValTarget(self):
        """(validation target images, validation target labels)"""
        val_target_dataset = tf.data.Dataset.from_tensor_slices((self.tgt_imgs_val, self.src_labels_val)).shuffle(
            1000).prefetch(tf.data.AUTOTUNE)
        return val_target_dataset

