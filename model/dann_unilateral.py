import tensorflow as tf
from tensorflow.keras.metrics import categorical_accuracy, binary_accuracy
from utils import GradientReversalLayer
from .dann import DANN
from .element import DANNElement

ele = DANNElement()
feature_extractor = ele.feature_extractor
label_predictor = ele.label_predictor
domain_predictor = ele.domain_predictor


class DANNUni(DANN):
    def __init__(self, training_mode='dann', pretrained_f=None, f=feature_extractor, l=label_predictor, d=domain_predictor):
        super(DANNUni, self).__init__()
        # choose our training step depending on the training mode
        self.train_step = tf.cond(training_mode == 'dann', lambda: self.train_step, lambda: self.train_step_src)

        self.pretrained_f = pretrained_f
        self.feature_extractor = f
        self.label_predictor = l
        self.domain_predictor = d

        self.feature_extractor.build([28, 28, 3])
        self.label_predictor.build([None, 768])
        self.domain_predictor.build([None, 768])

        self.lmda = 1.0

        self.clf_acc = categorical_accuracy
        self.d_acc = binary_accuracy
        self.grl = GradientReversalLayer()

        self.grl.build([None])

    def train_step(self, data):

        src_imgs, src_labels, tgt_imgs = data[0][0], data[0][1], data[1][0]
        batch_size = tf.shape(src_imgs)[0]
        # Combine source images and target images
        combined_img = tf.concat([src_imgs, tgt_imgs], axis=0)
        # Create labels for domain predictor,
        # 1 -> source images
        # 0 -> target images
        domain_labels = tf.concat([tf.ones((batch_size, 1)), tf.zeros((batch_size, 1))], axis=0)

        with tf.GradientTape() as tape:
            pretrained_src_features = self.pretrained_f(src_imgs, training=False)
            combined_features = self.feature_extractor(combined_img)

            src_features = combined_features[:batch_size]
            pred_labels = self.label_predictor(src_features)
            pred_loss = self.label_loss_fn(src_labels, pred_labels)
            pred_acc = self.clf_acc(src_labels, pred_labels)

            cons_loss = tf.reduce_mean(tf.abs(src_features - tf.stop_gradient(pretrained_src_features)))
            pred_domain = self.domain_predictor(self.grl(combined_features, self.lmda))
            domain_loss = self.domain_loss_fn(domain_labels, pred_domain)
            domain_acc = self.d_acc(domain_labels, pred_domain)

            loss = tf.reduce_mean(pred_loss) + tf.reduce_mean(domain_loss) + cons_loss

        vars = tape.watched_variables()
        grads = tape.gradient(loss, vars, unconnected_gradients=tf.UnconnectedGradients.ZERO)
        self.opt.apply_gradients(zip(grads, vars))

        self.train_loss.update_state(loss)
        self.train_clf_loss.update_state(pred_loss)
        self.train_domain_loss.update_state(domain_loss)

        self.train_clf_acc.update_state(pred_acc)
        self.train_domain_acc.update_state(domain_acc)

        return {"loss": self.train_loss.result(),
                "clf_loss": self.train_clf_loss.result(),
                "domain_loss": self.train_domain_loss.result(),
                "clf_acc": self.train_clf_acc.result(),
                "domain_acc": self.train_domain_acc.result()}

    def test_step(self, data):
        val_tgt_img, val_labels = data[0], data[1]
        batch_size = tf.shape(val_tgt_img)[0]
        tgt_domain_labels = tf.zeros((batch_size, 1))

        pretrained_tgt_features = self.pretrained_f(val_tgt_img, training=False)
        tgt_img_feature = self.feature_extractor(val_tgt_img, training=False)
        tgt_img_pred = self.label_predictor(tgt_img_feature, training=False)
        tgt_d_out = self.domain_predictor(tgt_img_feature, training=False)

        cons_loss = tf.reduce_mean(tf.abs(tgt_img_feature - tf.stop_gradient(pretrained_tgt_features)))
        pred_loss = self.label_loss_fn(val_labels, tgt_img_pred)
        pred_acc = self.clf_acc(val_labels, tgt_img_pred)
        domain_loss = self.domain_loss_fn(tgt_domain_labels, tgt_d_out)
        domain_acc = self.d_acc(tgt_domain_labels, tgt_d_out)

        loss = tf.reduce_mean(pred_loss) + tf.reduce_mean(domain_loss) + cons_loss

        self.val_loss.update_state(loss)
        self.val_clf_loss.update_state(pred_loss)
        self.val_domain_loss.update_state(domain_loss)

        self.val_clf_acc.update_state(pred_acc)
        self.val_domain_acc.update_state(domain_acc)

        return {"loss": self.val_loss.result(),
                "domain_acc": self.val_domain_acc.result(),
                "clf_acc": self.val_clf_acc.result()}












