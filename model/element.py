import tensorflow as tf
from tensorflow.keras import layers


class DANNElement(object):
    """Build the elements we need for the DANN and DANN-Uni models"""
    def __init__(self):
        self.feature_extractor = self._build_feature_extractor()
        self.label_predictor = self._build_label_predictor()
        self.domain_predictor = self._build_domain_predictor()

    @staticmethod
    def _build_feature_extractor():
        feature_extractor = tf.keras.Sequential(
            [
                tf.keras.Input(shape=(28, 28, 3)),
                layers.Conv2D(filters=32, kernel_size=5, strides=1, activation='relu'),
                layers.MaxPool2D(pool_size=(2, 2), strides=2),
                layers.Conv2D(filters=48, kernel_size=5, strides=1, activation='relu'),
                layers.MaxPool2D(pool_size=(2, 2), strides=2),
                layers.Flatten()
            ],
            name="feature_extractor",
        )
        return feature_extractor

    @staticmethod
    def _build_label_predictor():
        label_predictor = tf.keras.Sequential(
            [
                layers.Dense(100, activation='relu'),
                layers.Dense(100, activation='relu'),
                layers.Dense(10, activation='softmax')
            ],
            name="label_predictor"
        )
        return label_predictor

    @staticmethod
    def _build_domain_predictor():
        domain_predictor = tf.keras.Sequential(
            [
                layers.Dense(100, activation='relu'),
                layers.Dense(1)
            ],
            name="domain_predictor"
        )
        return domain_predictor
