# Evaluate the DANN+Unilateral model on the CWRU Bearing dataset
## Directory Structure
```
.
├── README.md
├── cwru_data
│   ├── data_cwru
│   └── dataloader_cwru.py
├── model_cwru
│   ├── __init__.py
│   ├── cwru_element.py
│   ├── dann_cwru.py
│   └── dannuni_cwru.py
├── run_cwru.py
└── utils_cwru.py

```


## Data Preparation
- Download the CWRU dataset [Link to Download](https://engineering.case.edu/bearingdatacenter/download-data-file)
- Change the names of the downloaded `*.mat` files and put into the `cwru_data/data_cwru` folder 

## Training
Arguments:
- `training_mode`:
    - dann: source data + target data for training
    - src:  only source data for training
- `model`:
    - DANN
    - DANNUni
- `src_load`: source data domain [0, 1, 2, 3]
- `tgt_load`: target data domain [0, 1, 2, 3]

For example: Task 0-1 using DANN+Unilateral model
```
python \
run_experiment.py \
--training_mode='dann' \
--model='DANNUni' \
--src_load=0 \
--tgt_load=1 \
--batch_size=128 \
--epochs=250 
```

## Results
| Setup | Baseline | DANN | DANN+Unilateral | 
| :---: | :---: | :---: | :---: | 
| Task 0-1 | 90.4 | 90.9 | 96.7 |
| Task 2-0 | 85.4 | 83.1 | 80.1 |
| Task 3-1 | 86.9 | 86.3 | 88.6 |

![CWRU_result](/uploads/0fecd1ce009814b7e6eae9c93871735f/CWRU_result.png)

## Reference
[`dataloader_cwru.py`](https://github.com/diagnosisda/dxda) from the original paper for data preprocessing

