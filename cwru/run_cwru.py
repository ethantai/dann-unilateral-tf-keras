from cwru_data.dataloader_cwru import load_cwru
from model_cwru import DANN4CWRU, DANNUni4CWRU
from utils_cwru import UpdateCallback4CWRU

import numpy as np
import tensorflow as tf
import argparse
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix

try:
    import wandb
except ModuleNotFoundError:
    pass


tf.random.set_seed(1)
np.random.seed(1)

EPOCHS = 250
BATCH_SIZE = 128
MODEL = "DANNUni"
MODE = "dann"


def _setup_parser():

    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument("--batch_size", type=int, default=BATCH_SIZE)
    parser.add_argument("--epochs", type=int, default=EPOCHS)
    parser.add_argument("--model", type=str, choices=["DANNUni", "DANN"], default=MODEL)
    parser.add_argument("--training_mode", type=str, choices=["src", "dann"], default=MODE)
    parser.add_argument("--src_load", type=int, choices=[0, 1, 2, 3], default=0)
    parser.add_argument("--tgt_load", type=int, choices=[0, 1, 2, 3], default=1)

    # weights and biases tool for tracking experiment
    parser.add_argument("--wandb", action="store_true", default=False)

    return parser


if __name__ == '__main__':
    parser = _setup_parser()
    args = parser.parse_args()

    # Load data
    data_src, label_src = load_cwru(args.src_load, truncate=120000)
    data_tgt, label_tgt = load_cwru(args.tgt_load, mode="20%", truncate=120000)
    data_val, label_val = load_cwru(args.tgt_load, truncate=120000)

    train_src_dataset = tf.data.Dataset.from_tensor_slices((data_src, label_src)).shuffle(1000)
    val_dataset = tf.data.Dataset.from_tensor_slices((data_val, label_val)).shuffle(1001).batch(args.batch_size).prefetch(tf.data.AUTOTUNE)

    ckpt_filepath = f"./training/{args.model}_{args.training_mode}_{args.src_load}to{args.tgt_load}/best_model"
    ckpt_callbacks = tf.keras.callbacks.ModelCheckpoint(
        filepath=ckpt_filepath,
        save_weights_only=True,
        monitor='val_clf_acc',
        mode='max',
        save_best_only=True)
    callbacks = [ckpt_callbacks]

    if args.wandb:
        # initialize wandb logging to your project
        wandb.init(project='dann-cwru', notes=None)
        # log all experimental args to wandb
        wandb.config.update(args)
        callbacks.append(wandb.keras.WandbCallback())

    eager_mode = False
    if args.training_mode == 'dann':
        """
        training dataset -> source data and target data
        The eager mode has to be set to True because we have to update the adaptation parameter(lambda) in the gradient
        reversal layer every epoch
        """
        train_tgt_dataset = tf.data.Dataset.from_tensor_slices((data_tgt, label_tgt)).shuffle(1000)
        train_dataset = tf.data.Dataset.zip((train_src_dataset, train_tgt_dataset.repeat())).batch(
            args.batch_size // 2).prefetch(tf.data.AUTOTUNE).shuffle(1001)
        eager_mode = True
        callbacks.append(UpdateCallback4CWRU(args.epochs))
    else:
        train_dataset = train_src_dataset.batch(args.batch_size)

    print(f"Model: {args.model}")
    print(f"Training Mode: {args.training_mode}")
    print(f"Eager Mode: {eager_mode}")
    print(f"Task {args.src_load} ---> {args.tgt_load}")

    if args.model == 'DANN':
        model = DANN4CWRU(training_mode=args.training_mode)
        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
            label_loss_fn=tf.keras.losses.SparseCategoricalCrossentropy(),
            domain_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            run_eagerly=eager_mode,
        )
        model.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=args.epochs,
            callbacks=callbacks
        )
    else:
        # Only source data for training the pretrained feature extractor
        src_data_train, src_data_val, src_label_train, src_label_val = train_test_split(data_src, label_src,
                                                                                        test_size=0.1,
                                                                                        random_state=1)
        train_source_dataset = tf.data.Dataset.from_tensor_slices((src_data_train, src_label_train)).shuffle(1000).batch(
            args.batch_size).prefetch(tf.data.AUTOTUNE)

        val_source_dataset = tf.data.Dataset.from_tensor_slices((src_data_val, src_label_val)).shuffle(1000).batch(
            args.batch_size).prefetch(tf.data.AUTOTUNE)

        pretrained_model = DANN4CWRU(training_mode="src")
        pretrained_model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
            label_loss_fn=tf.keras.losses.SparseCategoricalCrossentropy(),
            domain_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            run_eagerly=False,
        )
        print("========== Start training pretrained model ==========")
        pretrained_model.fit(
            train_source_dataset,
            validation_data=val_source_dataset,
            epochs=150
        )
        print("========== Pretrained feature extractor finished ==========")
        pretrained_feature_extractor = pretrained_model.feature_extractor
        model = DANNUni4CWRU(training_mode=args.training_mode, pretrained_f=pretrained_feature_extractor)
        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
            label_loss_fn=tf.keras.losses.SparseCategoricalCrossentropy(),
            domain_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            run_eagerly=eager_mode,
        )
        print("========== Start training DANN-Unilateral model ==========")
        model.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=args.epochs,
            callbacks=callbacks
        )
        print("========== Finished ==========")

    tgt_pred = model.predict(data_val)
    tgt_pred = tf.argmax(tgt_pred, axis=1).numpy()
    tgt_clf_acc = accuracy_score(label_val, tgt_pred)
    tgt_cm = confusion_matrix(label_val, tgt_pred)

    print(f"Evaluate the {args.model} model on the target data")
    print(f"Accuracy: {tgt_clf_acc}")
    print(f"Confusion Matrix:\n {tgt_cm}")

