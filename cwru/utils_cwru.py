import tensorflow as tf
from tensorflow.keras import layers


@tf.custom_gradient
def gradient_reversal(x, alpha=1.0):
    def grad(dy):
        return -dy * alpha, None

    return x, grad


class GradientReversalLayer(layers.Layer):
    def __init__(self, **kwargs):
        super(GradientReversalLayer, self).__init__(**kwargs)

    def call(self, x, alpha=1.0):

        return gradient_reversal(x, alpha)


class UpdateCallback4CWRU(tf.keras.callbacks.Callback):
    def __init__(self, total_epochs):
        self.total_epochs = total_epochs

    def on_epoch_begin(self, epoch, logs=None):
        """Update the adaptation parameter every epoch"""
        dtype = tf.float32
        process = tf.cast(epoch, dtype) / tf.cast(tf.constant(self.total_epochs), dtype)
        one = tf.cast(tf.constant(1), dtype)
        two = tf.cast(tf.constant(2), dtype)
        ten = tf.cast(tf.constant(10), dtype)
        denom = one + tf.exp(tf.multiply(-ten, process))
        self.model.lmda = (two / denom - one) * 0.01