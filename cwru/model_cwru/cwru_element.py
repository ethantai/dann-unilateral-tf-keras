import tensorflow as tf
from tensorflow.keras import layers


class Element4CWRU(object):
    """Build the elements of the DANN model and the DANN-Uni model for the CWRU dataset"""
    def __init__(self):
        self.feature_extractor = self._build_feature_extractor()
        self.label_predictor = self._build_label_predictor()
        self.domain_predictor = self._build_domain_predictor()

    @staticmethod
    def _build_feature_extractor():
        feature_extractor = tf.keras.Sequential(
            [
                tf.keras.Input(shape=(512, 1)),
                layers.Conv1D(filters=10, kernel_size=3, padding='same', activation='sigmoid'),
                layers.Dropout(0.5),
                layers.Conv1D(filters=10, kernel_size=3, padding='same', activation='sigmoid'),
                layers.Dropout(0.5),
                layers.Conv1D(filters=10, kernel_size=3, padding='same', activation='sigmoid'),
                layers.Dropout(0.5),
                layers.Flatten(),
                layers.Dense(256, activation='sigmoid')
            ],
            name="feature_extractor",
        )
        return feature_extractor

    @staticmethod
    def _build_label_predictor():
        label_predictor = tf.keras.Sequential(
            [
                layers.Dense(256, activation='relu'),
                layers.Dropout(0.5),
                layers.Dense(10, activation='softmax')
            ],
            name="label_predictor"
        )
        return label_predictor

    @staticmethod
    def _build_domain_predictor():
        domain_predictor = tf.keras.Sequential(
            [
                layers.Dense(1024, activation='relu'),
                layers.Dense(1024, activation='relu'),
                layers.Dense(1)
            ],
            name="domain_predictor"
        )
        return domain_predictor
