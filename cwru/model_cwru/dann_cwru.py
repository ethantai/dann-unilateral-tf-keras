import tensorflow as tf
from tensorflow.keras.metrics import sparse_categorical_accuracy, binary_accuracy
from utils_cwru import GradientReversalLayer
from .cwru_element import Element4CWRU

ele = Element4CWRU()
feature_extractor = ele.feature_extractor
label_predictor = ele.label_predictor
domain_predictor = ele.domain_predictor


class DANN4CWRU(tf.keras.Model):
    def __init__(self, training_mode='dann', f=feature_extractor, l=label_predictor, d=domain_predictor, **kwargs):
        super(DANN4CWRU, self).__init__(**kwargs)
        # choose our training step depending on the training mode
        self.train_step = tf.cond(training_mode == 'dann', lambda: self.train_step, lambda: self.train_step_src)

        self.feature_extractor = f
        self.label_predictor = l
        self.domain_predictor = d

        self.feature_extractor.build([512, 1])
        self.label_predictor.build([None, 256])
        self.domain_predictor.build([None, 256])

        self.lmda = 1.0

        self.clf_acc = sparse_categorical_accuracy
        self.d_acc = binary_accuracy
        self.grl = GradientReversalLayer()

        self.grl.build([None])

    def compile(self, optimizer, label_loss_fn, domain_loss_fn, **kwargs):
        super(DANN4CWRU, self).compile(**kwargs)
        self.opt = optimizer
        self.label_loss_fn = label_loss_fn
        self.domain_loss_fn = domain_loss_fn

        self.train_loss = tf.keras.metrics.Mean("train_loss", dtype=tf.float32)
        self.train_clf_loss = tf.keras.metrics.Mean("train_clf_loss", dtype=tf.float32)
        self.train_domain_loss = tf.keras.metrics.Mean("train_domain_loss", dtype=tf.float32)
        self.train_clf_acc = tf.keras.metrics.Mean("train_clf_acc", dtype=tf.float32)
        self.train_domain_acc = tf.keras.metrics.Mean("train_domain_acc", dtype=tf.float32)

        self.val_loss = tf.keras.metrics.Mean("val_loss", dtype=tf.float32)
        self.val_clf_loss = tf.keras.metrics.Mean("val_clf_loss", dtype=tf.float32)
        self.val_domain_loss = tf.keras.metrics.Mean("val_domain_loss", dtype=tf.float32)
        self.val_clf_acc = tf.keras.metrics.Mean("val_clf_acc", dtype=tf.float32)
        self.val_domain_acc = tf.keras.metrics.Mean("val_domain_acc", dtype=tf.float32)

    @property
    def metrics(self):
        return [self.train_loss,
                self.train_clf_loss,
                self.train_domain_loss,
                self.train_clf_acc,
                self.train_domain_acc,
                self.val_loss,
                self.val_clf_loss,
                self.val_domain_loss,
                self.val_clf_acc,
                self.val_domain_acc]

    def train_step(self, data):
        """
        Custom training step for training mode is dann
        """

        src_data, src_labels, tgt_data = data[0][0], data[0][1], data[1][0]
        batch_size = tf.shape(src_data)[0]

        # Create labels for domain predictor,
        # 1 -> source data
        # 0 -> target data
        domain_labels = tf.concat([tf.ones((batch_size, 1)), tf.zeros((batch_size, 1))], axis=0)

        with tf.GradientTape() as tape:

            src_features = self.feature_extractor(src_data, training=True)
            tgt_features = self.feature_extractor(tgt_data, training=True)
            # Combine source features and target features
            combined_features = tf.concat([src_features, tgt_features], axis=0)

            pred_labels = self.label_predictor(src_features, training=True)
            pred_loss = self.label_loss_fn(src_labels, pred_labels)
            pred_acc = self.clf_acc(src_labels, pred_labels)

            pred_domain = self.domain_predictor(self.grl(combined_features, self.lmda), training=True)
            domain_loss = self.domain_loss_fn(domain_labels, pred_domain)
            domain_acc = self.d_acc(domain_labels, pred_domain)

            loss = tf.reduce_mean(pred_loss) + tf.reduce_mean(domain_loss)

        vars = tape.watched_variables()
        grads = tape.gradient(loss, vars)
        self.opt.apply_gradients(zip(grads, vars))

        self.train_loss.update_state(loss)
        self.train_clf_loss.update_state(pred_loss)
        self.train_domain_loss.update_state(domain_loss)

        self.train_clf_acc.update_state(pred_acc)
        self.train_domain_acc.update_state(domain_acc)

        return {"loss": self.train_loss.result(),
                "clf_loss": self.train_clf_loss.result(),
                "domain_loss": self.train_domain_loss.result(),
                "clf_acc": self.train_clf_acc.result(),
                "domain_acc": self.train_domain_acc.result()}

    @tf.function
    def train_step_src(self, data):
        """
        Custom training step for training mode is source.
        We do not need to train the domain predictor when our training dataset only has source data.
        """
        src_data, labels = data[0], data[1]

        with tf.GradientTape() as tape:
            features = self.feature_extractor(src_data, training=True)

            pred_labels = self.label_predictor(features, training=True)
            pred_loss = self.label_loss_fn(labels, pred_labels)
            pred_acc = self.clf_acc(labels, pred_labels)

            loss = tf.reduce_mean(pred_loss)

        vars = tape.watched_variables()
        grads = tape.gradient(loss, vars)
        self.opt.apply_gradients(zip(grads, vars))

        self.train_loss.update_state(loss)
        self.train_clf_loss.update_state(pred_loss)
        self.train_clf_acc.update_state(pred_acc)

        return {"loss": self.train_loss.result(),
                "clf_loss": self.train_clf_loss.result(),
                "clf_acc": self.train_clf_acc.result()}

    def test_step(self, data):

        val_tgt_img, val_labels = data[0], data[1]
        batch_size = tf.shape(val_tgt_img)[0]
        tgt_domain_labels = tf.zeros((batch_size, 1))

        tgt_img_feature = self.feature_extractor(val_tgt_img, training=False)
        tgt_img_pred = self.label_predictor(tgt_img_feature, training=False)
        tgt_d_out = self.domain_predictor(tgt_img_feature, training=False)

        pred_loss = self.label_loss_fn(val_labels, tgt_img_pred)
        pred_acc = self.clf_acc(val_labels, tgt_img_pred)
        domain_loss = self.domain_loss_fn(tgt_domain_labels, tgt_d_out)
        domain_acc = self.d_acc(tgt_domain_labels, tgt_d_out)

        loss = tf.reduce_mean(pred_loss) + tf.reduce_mean(domain_loss)

        self.val_loss.update_state(loss)
        self.val_clf_loss.update_state(pred_loss)
        self.val_domain_loss.update_state(domain_loss)

        self.val_clf_acc.update_state(pred_acc)
        self.val_domain_acc.update_state(domain_acc)

        return {"loss": self.val_loss.result(),
                "domain_acc": self.val_domain_acc.result(),
                "clf_acc": self.val_clf_acc.result()}

    def predict(self, data):
        tgt_feature = self.feature_extractor.predict(data)
        tgt_pred = self.label_predictor.predict(tgt_feature)
        return tgt_pred









